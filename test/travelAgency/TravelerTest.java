package travelAgency;

import java.math.BigDecimal;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;

/**
 *
 * @author gwolf
 */
public class TravelerTest {

    static Traveler instance = null;
    
    public TravelerTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
        instance = new Traveler();
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
        instance = null;
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    /**
     * Test of getName method, of class Traveler.
     */
    @Test
    public void testGetName() {
        String expResult = "Tom";
        String result = instance.getName();
        assertEquals(expResult, result);
    }

    /**
     * Test of getSurname method, of class Traveler.
     */
    @Test
    public void testGetSurname() {
        String expResult = "Turbo";
        String result = instance.getSurname();
        assertEquals(expResult, result);
    }

    /**
     * Test of constructor with first argument null, of class Traveler.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testConstructorTravelerNullArgument() {
        Traveler test = new Traveler(null);
    }

    /**
     * Test of constructor with first argument empty string, of class Traveler.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testConstructorTravelerEmptyStringArgument() {
        Traveler test = new Traveler("");
    }

    /**
     * Test of constructor with first argument empty string, of class Traveler.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testConstructorTravelerStringWithSpaces() {
        Traveler test = new Traveler("  ");
    }
    
    /**
     * Test of getBudget method, of class Traveler.
     */
    @Test
    public void testGetBudget() {
        BigDecimal expResult = new BigDecimal("100");
        BigDecimal result = instance.getBudget();
        assertEquals(expResult, result);
    }
    
    /**
     * Test of toString method, of class Traveler.
     */
    @Test
    public void testToString() {
        String expResult = "Tom Turbo has planed a budget of 100,00 € "
                + "for the journey.";
        String result = instance.toString();
        assertEquals(expResult, result);
    }

}
