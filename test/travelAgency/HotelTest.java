package travelAgency;

import java.util.List;
import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class HotelTest {

    Hotel h1;

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void generateHotel() {
        h1 = new Hotel("Arcotel");
    }

    @After
    public void cleanUp() {
        h1 = null;
        System.out.print("\n\n");
    }

    @Test
    public void testBookRoomStandard() {
        System.out.println("--> testBookRoom()");

        boolean expected = true;
        boolean actual = h1.bookRoom(RoomCategory.STANDARD);

        assertEquals(expected, actual);
    }

    @Test
    public void testBookRoomNoFreeStandardRoom() {
        System.out.println("--> testBookRoomNoFreeStandardRoom()");

        boolean expected = false;

        for (int i = 0; i < h1.getRoomsCount(); i++) {
            h1.bookRoom(RoomCategory.STANDARD);
        }

        boolean actual = h1.bookRoom(RoomCategory.STANDARD);

        assertEquals(expected, actual);
    }

    /**
     * Test of getName method, of class Hotel.
     */
    @Test
    public void testGetName() {
        System.out.println("getName");
        Hotel instance = new Hotel();
        String expResult = "A Hotel";
        String result = instance.getName();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetSpecialName() {
        System.out.println("getName");
        Hotel instance = new Hotel("A special Hotel");
        String expResult = "A special Hotel";
        String result = instance.getName();
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getStars method, of class Hotel.
     */
    @Test
    public void testGetStars() {
        System.out.println("getStars");
        Hotel instance = new Hotel("A Hotel");
        HotelCategory expResult = HotelCategory.STARS_3;
        HotelCategory result = instance.getStars();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetOtherStars() {
        System.out.println("getStars");
        Hotel instance = new Hotel("A Hotel", HotelCategory.STARS_4);
        HotelCategory expResult = HotelCategory.STARS_4;
        HotelCategory result = instance.getStars();
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getRoomsCount method, of class Hotel.
     */
    @Test
    public void testGetRoomsCount() {
        System.out.println("getRoomsCount");
        Hotel instance = new Hotel();
        int expResult = 5;
        int result = instance.getRoomsCount();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetSpecialRoomsCount() {
        System.out.println("getRoomsCount");
        Hotel instance = new Hotel("A special Hotel", HotelCategory.STARS_4, 7);
        int expResult = 7;
        int result = instance.getRoomsCount();
        assertEquals(expResult, result);
    }

    
    /**
     * Test of getRooms method, of class Hotel.
     */
    @Test
    public void testGetRooms() {
        System.out.println("getRooms");
        Hotel instance = new Hotel();
        List<Hotel.Room> expResult = null;
        List<Hotel.Room> result = instance.getRooms();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of generateRooms method, of class Hotel.
     */
    @Test
    public void testGenerateRooms() {
        System.out.println("generateRooms");
        RoomCategory roomCategory = null;
        int roomCount = 0;
        Hotel instance = new Hotel();
        instance.generateRooms(roomCategory, roomCount);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setRooms method, of class Hotel.
     */
    @Test
    public void testSetRooms() {
        System.out.println("setRooms");
        HotelCategory hotelCategory = null;
        int roomsCount = 0;
        Hotel instance = new Hotel();
        instance.setRooms(hotelCategory, roomsCount);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of bookRoom method, of class Hotel.
     */
    @Test
    public void testBookRoom_RoomCategory() {
        System.out.println("bookRoom");
        RoomCategory roomCategory = null;
        Hotel instance = new Hotel();
        boolean expResult = false;
        boolean result = instance.bookRoom(roomCategory);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of bookRoom method, of class Hotel.
     */
    @Test
    public void testBookRoom_RoomCategory_Traveler() {
        System.out.println("bookRoom");
        RoomCategory roomCategory = null;
        Traveler guest = null;
        Hotel instance = new Hotel();
        boolean expResult = false;
        boolean result = instance.bookRoom(roomCategory, guest);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of toString method, of class Hotel.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Hotel instance = new Hotel();
        String expResult = "";
        String result = instance.toString();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

}
