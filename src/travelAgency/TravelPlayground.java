package travelAgency;

import java.math.BigDecimal;
import java.text.NumberFormat;

public class TravelPlayground {

    public static void main(String[] args) {

        // Tests
        System.out.println("..:: TravelAgency ::..");
        System.out.println(">> My learning project during the SCJP course.\n"
                + "   I want to simulate a trip. Hotels, pricing, booking, etc.\n"
                + "   Realized by a travel agency for a traveler.");

        System.out.println("---------------------");

        // Traveler
        Traveler t1 = new Traveler("Angus", "MacGyver", new BigDecimal("300"));
        System.out.println(t1);

        System.out.println("---------------------");

        // Hotel
        Hotel h1 = new Hotel("Arcotel", HotelCategory.STARS_2, 1);
        System.out.println(h1);

        System.out.println();

        Hotel h2 = new Hotel("Ritz Carlton", HotelCategory.STARS_5, 50);
        System.out.println(h2);

        System.out.println("---------------------");

        // RoomCategories
        System.out.printf("A room of the category %s costs %s per night.%n",
                RoomCategory.STANDARD.name(), NumberFormat.getCurrencyInstance()
                .format(RoomCategory.STANDARD.getCost()));

        System.out.println("---------------------");

        // let's reserve a standard room
        h1.bookRoom(RoomCategory.STANDARD);

        System.out.println("---------------------");

        // Utility
        System.out.println(Utility.getToday());
        System.out.println(Utility.getDate("16.5.2004"));

        System.out.println("---------------------");

        // Hotel has a schedule, let's fill it with one booking
        // h1.scheduleBooking(Utility.getDate("17.06.1984"),
        // h1.getRooms().get(2));
    }

}
