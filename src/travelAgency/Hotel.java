package travelAgency;

import java.util.ArrayList;
import java.util.List;

public class Hotel {

    private String name;
    private HotelCategory stars;
    private int roomsCount;
    private List<Room> rooms = new ArrayList<>();

    public String getName() {
        return name;
    }

    private void setName(String name) {
        this.name = name;
    }

    public HotelCategory getStars() {
        return stars;
    }

    private void setStars(HotelCategory stars) {
        this.stars = stars;
    }

    public int getRoomsCount() {
        return roomsCount;
    }

    private void setRoomsCount(int roomsCount) {
        if (roomsCount < 1) {
            throw new IllegalArgumentException(
                    roomsCount + " A Hotel without rooms makes no sense.");
        }
        this.roomsCount = roomsCount;
    }

    public List<Room> getRooms() {
        return rooms;
    }

    public void generateRooms(RoomCategory roomCategory, int roomCount) {
        for (int i = 0; i < roomCount; i++) {
            rooms.add(new Room(rooms.size() + 1, roomCategory));
        }

    }

    public void setRooms(HotelCategory hotelCategory, int roomsCount) {
        int dormitoryRooms = (int) ((double) roomsCount / 100
                * hotelCategory.getDormitory());
        int standardRooms = (int) ((double) roomsCount / 100
                * hotelCategory.getStandard());
        int premiumRooms = (int) ((double) roomsCount / 100
                * hotelCategory.getPremium());
        int suiteRooms = (int) ((double) roomsCount / 100
                * hotelCategory.getSuite());

        int sum = dormitoryRooms + standardRooms + premiumRooms + suiteRooms;
        System.out.println("--> Debug: Generate rooms expected (" + roomsCount
                + ") actual (" + sum + ")");

        generateRooms(RoomCategory.DORMITORY, dormitoryRooms);
        generateRooms(RoomCategory.STANDARD, standardRooms);
        generateRooms(RoomCategory.PREMIUM, premiumRooms);
        generateRooms(RoomCategory.SUITE, suiteRooms);
    }

    public boolean bookRoom(RoomCategory roomCategory) {
        return bookRoom(roomCategory, new Traveler());
    }

    public boolean bookRoom(RoomCategory roomCategory, Traveler guest)
            throws IllegalArgumentException {

        if (roomCategory == null) {
            throw new IllegalArgumentException(
                    "To book a room, you must specify a RoomCategory");
        }

        System.out.println("Booking a " + roomCategory + " room...");

        for (Room room : getRooms()) {
            if (room.getRoomCategory() == roomCategory
                    && room.isFree() == true) {
                getRooms().get(room.getRoomNumber() - 1).setGuest(guest);
                System.out.printf("Room #%d booked for %s %s%n",
                        room.getRoomNumber(), guest.getName(),
                        guest.getSurname());

                return true;
            }
        }
        System.out.println("Sorry. No free room availible.");
        return false;
    }

    // constructors
    public Hotel() {
        this("A Hotel");
    }

    public Hotel(String name) {
        this(name, HotelCategory.STARS_3, 5);
    }

    public Hotel(String name, HotelCategory stars) {
        this(name, stars, 5);
    }

    public Hotel(String name, HotelCategory stars, int roomsCount) {
        setName(name);
        setStars(stars);
        setRoomsCount(roomsCount);
        setRooms(stars, roomsCount);
    }

    @Override
    public String toString() {
        int dormitory = 0;
        int standard = 0;
        int premium = 0;
        int suite = 0;

        for (Room room : getRooms()) {
            if (room.roomCategory == RoomCategory.DORMITORY) {
                dormitory++;
            }
            if (room.roomCategory == RoomCategory.STANDARD) {
                standard++;
            }
            if (room.roomCategory == RoomCategory.PREMIUM) {
                premium++;
            }
            if (room.roomCategory == RoomCategory.SUITE) {
                suite++;
            }
        }

        return String.format(
                "The %s «%s» has these rooms: %n %2d Dormitory %n %2d Standard"
                + " %n %2d Premium %n %2d Suite",
                getStars().toString(), getName(), dormitory, standard, premium,
                suite);
    }

    class Room implements Queryable {

        private int roomNumber;
        private RoomCategory roomCategory;
        private Traveler guest;

        // getters and setters
        public int getRoomNumber() {
            return roomNumber;
        }

        public void setRoomNumber(int roomNumber) {
            this.roomNumber = roomNumber;
        }

        public RoomCategory getRoomCategory() {
            return roomCategory;
        }

        public void setRoomCategory(RoomCategory roomCategory) {
            this.roomCategory = roomCategory;
        }

        public Traveler getGuest() {
            return guest;
        }

        public void setGuest(Traveler guest) {
            this.guest = guest;
        }

        // constructors
        public Room() {
            this(0);
        }

        public Room(int roomNumber) {
            this(roomNumber, RoomCategory.STANDARD, null);
        }

        public Room(int roomNumber, RoomCategory roomCategory) {
            this(roomNumber, roomCategory, null);
        }

        public Room(int roomNumber, RoomCategory roomCategory, Traveler guest) {
            setRoomNumber(roomNumber);
            setRoomCategory(roomCategory);
            setGuest(guest);
        }

        @Override
        public String toString() {
            String freeOrNot = getGuest() != null ? getGuest().toString()
                    : "free";
            return "Room #" + roomNumber + " category " + roomCategory + " ("
                    + freeOrNot + ")";
        }

        @Override
        public boolean isFree() {
            return getGuest() == null;
        }

    }

}
