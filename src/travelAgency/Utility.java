package travelAgency;

import java.util.Calendar;
import java.util.Date;

public class Utility {

    public static Date getToday() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        Date dateWithoutTime = cal.getTime();
        return dateWithoutTime;
    }

    // accepts a date in the form dd.mm.yyyy
    // and returns a date object the same day at 00:00:00 h
    public static Date getDate(String date) {
        String[] dateParts = date.split("\\.");
        Calendar cal = Calendar.getInstance();
        cal.set(Integer.parseInt(dateParts[2]), Integer.parseInt(dateParts[1]),
                Integer.parseInt(dateParts[0]), 0, 0, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

}
