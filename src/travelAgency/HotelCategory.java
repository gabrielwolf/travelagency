package travelAgency;

public enum HotelCategory {
    STARS_1(50, 50, 0, 0), STARS_2(40, 40, 20, 0), STARS_3(30, 40, 30,
            0), STARS_4(10, 30, 40, 20), STARS_5(0, 10, 50, 40);

    private int dormitory;
    private int standard;
    private int premium;
    private int suite;

    public int getDormitory() {
        return dormitory;
    }

    private final void setDormitory(int dormitory) {
        this.dormitory = dormitory;
    }

    public int getStandard() {
        return standard;
    }

    private final void setStandard(int standard) {
        this.standard = standard;
    }

    public int getPremium() {
        return premium;
    }

    private final void setPremium(int premium) {
        this.premium = premium;
    }

    public int getSuite() {
        return suite;
    }

    private final void setSuite(int suite) {
        this.suite = suite;
    }

    HotelCategory(int dormitory, int standard, int premium, int suite) {
        setDormitory(dormitory);
        setStandard(standard);
        setPremium(premium);
        setSuite(suite);
    }

    public String toString() {
        String stars = "";
        for (int i = 0; i <= this.ordinal(); i++) {
            stars += "*";
        }
        return stars;
    }
}
