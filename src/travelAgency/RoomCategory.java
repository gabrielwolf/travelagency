package travelAgency;

import java.math.BigDecimal;

public enum RoomCategory {
    DORMITORY(new BigDecimal("12.00")), STANDARD(
            new BigDecimal("64.00")), PREMIUM(
            new BigDecimal("127.00")), SUITE(new BigDecimal("2450.00"));

    private BigDecimal cost;

    public BigDecimal getCost() {
        return cost;
    }

    RoomCategory(BigDecimal cost) {
        this.cost = cost;
    }

    @Override
    public String toString() {
        return name();
    }
}
