package travelAgency;

import java.util.Date;
import java.util.TreeMap;

import travelAgency.Hotel.Room;

public class Booking {

    private TreeMap<Date, Room> schedule = new TreeMap<Date, Room>();

    public void setSchedule(TreeMap<Date, Room> schedule) {
        this.schedule = schedule;
    }

    public void scheduleBooking(Date date, Room room) {
        schedule.put(date, room);
        System.out.println(schedule);
    }

}
