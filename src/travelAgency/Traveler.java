package travelAgency;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Traveler {

    private String name;
    private String surname;
    private BigDecimal budget;

    private static boolean isName(String name) {
        if (name != null) {
            
            /* Test if given argument is a Name, from:
             * https://stackoverflow.com/questions/275160/regex-for-names#2044909
             * 
             * Mandatory single name, optional additional names,
             * WITH spaces, WITHOUT special characters:
             * - Jack Alexander is valid
             * - Sierra O'Neil is invalid (has a special character)
             */
            String pattern = "^[A-Za-z]+((\\s)?([A-Za-z])+)*$";
            
            Pattern p = Pattern.compile(pattern);
            Matcher m = p.matcher(name);
            return m.find();
        } else {
            return false;
        }
    }

    public String getName() {
        return name;
    }

    private void setName(String name) throws IllegalArgumentException {
        if (!isName(name)) {
            throw new IllegalArgumentException("A name must be given.");
        }
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    private void setSurname(String surname) throws IllegalArgumentException {
        if (!isName(surname)) {
            throw new IllegalArgumentException("A surname must be given.");
        }
        this.surname = surname;
    }

    public BigDecimal getBudget() {
        return budget;
    }

    private void setBudget(BigDecimal budget) {
        this.budget = budget;
    }

    // constructors
    public Traveler() {
        this("Tom");
    }

    public Traveler(String name) {
        this(name, "Turbo", new BigDecimal("100"));
    }

    public Traveler(String name, String surname) {
        this(name, surname, new BigDecimal("100"));
    }

    public Traveler(String name, String surname, BigDecimal budget) {
        setName(name);
        setSurname(surname);
        setBudget(budget);
    }

    @Override
    public String toString() {
        return getName() + " " + getSurname() + " has planed a budget of "
                + NumberFormat.getCurrencyInstance().format(getBudget())
                + " for the journey.";
    }

}
